
# OnePlace

## Visão Geral

**OnePlace** é uma aplicação desenvolvida para gerenciar informações relacionadas à saúde e bem-estar de pacientes. O sistema permite o registro, atualização e exclusão de dados sobre atendimentos, medicações, médicos e pacientes. O projeto foi desenvolvido como parte do curso de Engenharia de Software, tendo como objetivo facilitar a gestão de dados em clínicas e hospitais.

## Equipe de Desenvolvimento

- Monique Pavan
- Renan Henriques
- Tainara da Rosa

## Estrutura do Projeto

O projeto está organizado em diferentes pacotes e camadas, seguindo o padrão MVC (Model-View-Controller):

- **Controllers**: Contém os controladores responsáveis por receber as requisições HTTP e direcioná-las aos serviços apropriados.
- **Models**: Contém as classes de modelo que representam as entidades do sistema (Atendimento, Medicacao, Medico, Paciente).
- **Repositories**: Contém as interfaces que interagem com o banco de dados.
- **Services**: Contém as classes de serviço que implementam a lógica de negócio da aplicação.

## Funcionalidades

### Ajuda

Endpoint: `/ajuda`

Método: `GET`

Retorna informações sobre os desenvolvedores e o projeto.


## Configuração

O projeto estará disponível em `http://localhost:8080`.

## Endpoints Disponíveis

### Ajuda

- **GET** `/ajuda` - Retorna informações sobre o projeto e desenvolvedores.

### Atendimentos

- **GET** `/atendimentos` - Retorna uma lista de atendimentos médicos.
  - Parâmetros de consulta: `page`, `size`
- **GET** `/atendimentos/{id}` - Retorna um atendimento específico por ID.
- **POST** `/atendimentos` - Cria um novo atendimento.
  - Corpo da requisição: JSON com detalhes do atendimento.
- **PUT** `/atendimentos/{id}` - Atualiza um atendimento existente por ID.
  - Corpo da requisição: JSON com detalhes atualizados do atendimento.
- **DELETE** `/atendimentos/{id}` - Exclui um atendimento por ID.

### Medicacoes

- **GET** `/medicacoes` - Retorna uma lista de medicações.
  - Parâmetros de consulta: `page`, `size`
- **GET** `/medicacoes/{id}` - Retorna uma medicação específica por ID.
- **POST** `/medicacoes` - Cria uma nova medicação.
  - Corpo da requisição: JSON com detalhes da medicação.
- **PUT** `/medicacoes/{id}` - Atualiza uma medicação existente por ID.
  - Corpo da requisição: JSON com detalhes atualizados da medicação.
- **DELETE** `/medicacoes/{id}` - Exclui uma medicação por ID.

### Medicos

- **GET** `/medicos` - Retorna uma lista de médicos.
  - Parâmetros de consulta: `page`, `size`
- **GET** `/medicos/{id}` - Retorna um médico específico por ID.
- **POST** `/medicos` - Cria um novo médico.
  - Corpo da requisição: JSON com detalhes do médico.
- **PUT** `/medicos/{id}` - Atualiza um médico existente por ID.
  - Corpo da requisição: JSON com detalhes atualizados do médico.
- **DELETE** `/medicos/{id}` - Exclui um médico por ID.

### Pacientes

- **GET** `/api/pacientes` - Retorna uma lista de pacientes.
  - Parâmetros de consulta: `page`, `size`
- **GET** `/api/pacientes/{id}` - Retorna um paciente específico por ID.
- **POST** `/api/pacientes` - Cria um novo paciente.
  - Corpo da requisição: JSON com detalhes do paciente.
- **PUT** `/api/pacientes/{id}` - Atualiza um paciente existente por ID.
  - Corpo da requisição: JSON com detalhes atualizados do paciente.
- **DELETE** `/api/pacientes/{id}` - Exclui um paciente por ID.

## Exemplos GET

**/api/pacientes**

{
  "nomePaciente": "Maria Joana", 
  "cpfPaciente": "11555596375",
  "dataNascimentoPaciente": "1967-01-15T00:00:00.000+00:00"
}

{
  "nomePaciente": "Lucas Lopes", 
  "cpfPaciente": "12956999915",
  "dataNascimentoPaciente": "1988-09-10T00:00:00.000+00:00"
}

{
  "nomePaciente": "Marcos Pacheco", 
  "cpfPaciente": "96315598371",
  "dataNascimentoPaciente": "2001-06-17T13:13:32.351+00:00"
}


**/medicos**

{
  "nomeMedico": "Wilson Gonçalves", 
  "numeroCRM": 963255,
  "especializacaoMedico": "Cardiologista"
}

{
  "nomeMedico": "Ana Luiza Bento", 
  "numeroCRM": 63598,
  "especializacaoMedico": "Dermatologista"
}

{
  "nomeMedico": "Renan Duarte", 
  "numeroCRM": 63598,
  "especializacaoMedico": "Clínico Geral"
}

**/medicacoes**

{
  "nomeMedicacao": "Dramin", 
  "fabricante": "Takeda",
  "dosagem": "30ml",
  "validadeMedicacao": "2026-09-10T13:13:32.351+00:00"
}

{
  "nomeMedicacao": "Paracetamol", 
  "fabricante": "Neoquímica",
  "dosagem": "750mg",
  "validadeMedicacao": "2025-06-10T13:13:32.351+00:00"
}

{
  "nomeMedicacao": "Dipirona", 
  "fabricante": "Donaduzzi",
  "dosagem": "100mg",
  "validadeMedicacao": "2026-07-15T13:13:32.351+00:00"
}

**/atendimentos**

{
	"paciente": {
		"idPaciente": 2,
		"nomePaciente": null,
		"cpfPaciente": null,
		"dataNascimentoPaciente": null
	},
	"medico": {
		"idMedico": 2,
		"nomeMedico": null,
		"numeroCRM": null,
		"especializacaoMedico": null
	},
	"dataAtendimento": "2024-06-15T10:00:00.000+00:00",
	"descricaoAtendimento": "Retorno",
	"medicacao": {
		"idMedicacao": 2,
		"nomeMedicacao": null,
		"fabricante": null,
		"dosagem": null,
		"validadeMedicacao": null
	}
}

##Docker

docker ps

docker logs 5158e7cda4ac

docker stop 5158e7cda4ac


##Testes unitários

cd C:caminho da pasta

mvnw test

