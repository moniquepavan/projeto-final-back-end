package com.example.saudeBemEstar.models;

import org.junit.jupiter.api.Test;
import java.util.Date;
import static org.junit.jupiter.api.Assertions.*;

public class PacienteTest {

    @Test
    public void testCpfValido() {
        Paciente paciente = new Paciente(1L, "Maria Joana", "11555596375", new Date());
        assertTrue(paciente.isCpfValido());
    }

    @Test
    public void testCpfInvalido() {
        Paciente paciente = new Paciente(1L, "Maria Joana", "12345", new Date());
        assertFalse(paciente.isCpfValido());
    }

    @Test
    public void testCpfNulo() {
        Paciente paciente = new Paciente(1L, "Maria Joana", null, new Date());
        assertFalse(paciente.isCpfValido());
    }
}
