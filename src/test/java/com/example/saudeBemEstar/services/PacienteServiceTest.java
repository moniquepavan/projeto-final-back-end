package com.example.saudeBemEstar.services;

import com.example.saudeBemEstar.models.Paciente;
import com.example.saudeBemEstar.repositories.PacienteRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class PacienteServiceTest {

    @InjectMocks
    private PacienteService pacienteService;

    @Mock
    private PacienteRepository pacienteRepository;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testFindAll() {
        List<Paciente> pacientes = new ArrayList<>();
        pacientes.add(new Paciente(1L, "Maria Joana", "11555596375", new Date()));
        pacientes.add(new Paciente(2L, "Lucas Lopes", "12956999915", new Date()));
        Page<Paciente> page = new PageImpl<>(pacientes);

        when(pacienteRepository.findAll(any(PageRequest.class))).thenReturn(page);

        List<Paciente> result = pacienteService.findAll(0, 10);
        assertEquals(2, result.size());
        verify(pacienteRepository, times(1)).findAll(any(PageRequest.class));
    }

    @Test
    public void testFindById() {
        Paciente paciente = new Paciente(1L, "Maria Joana", "11555596375", new Date());
        when(pacienteRepository.findById(1L)).thenReturn(Optional.of(paciente));

        Paciente result = pacienteService.findById(1L);
        assertNotNull(result);
        assertEquals("Maria Joana", result.getNomePaciente());
        verify(pacienteRepository, times(1)).findById(1L);
    }

    @Test
    public void testFindById_NotFound() {
        when(pacienteRepository.findById(1L)).thenReturn(Optional.empty());

        assertThrows(RuntimeException.class, () -> {
            pacienteService.findById(1L);
        });
    }

    @Test
    public void testSave() {
        Paciente paciente = new Paciente(1L, "Maria Joana", "11555596375", new Date());
        when(pacienteRepository.save(paciente)).thenReturn(paciente);

        Paciente result = pacienteService.save(paciente);
        assertNotNull(result);
        assertEquals("Maria Joana", result.getNomePaciente());
        verify(pacienteRepository, times(1)).save(paciente);
    }

    @Test
    public void testUpdate() {
        Paciente existingPaciente = new Paciente(1L, "Maria Joana", "11555596375", new Date());
        Paciente updatedPaciente = new Paciente(1L, "Maria Joana Atualizada", "11555596375", new Date());

        when(pacienteRepository.findById(1L)).thenReturn(Optional.of(existingPaciente));
        when(pacienteRepository.save(existingPaciente)).thenReturn(existingPaciente);

        Paciente result = pacienteService.update(1L, updatedPaciente);
        assertNotNull(result);
        assertEquals("Maria Joana Atualizada", result.getNomePaciente());
        verify(pacienteRepository, times(1)).findById(1L);
        verify(pacienteRepository, times(1)).save(existingPaciente);
    }

    @Test
    public void testDelete() {
        doNothing().when(pacienteRepository).deleteById(1L);

        pacienteService.delete(1L);
        verify(pacienteRepository, times(1)).deleteById(1L);
    }
}
