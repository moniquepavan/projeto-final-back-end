package com.example.saudeBemEstar.controllers;

import com.example.saudeBemEstar.models.Paciente;
import com.example.saudeBemEstar.services.PacienteService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class PacienteControllerTest {

    @InjectMocks
    private PacienteController pacienteController;

    @Mock
    private PacienteService pacienteService;

    private MockMvc mockMvc;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(pacienteController).build();
    }

    @Test
    public void testGetAllPacientes() throws Exception {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        Date date1 = sdf.parse("1967-01-15");
        Date date2 = sdf.parse("1988-09-10");
        Date date3 = sdf.parse("2001-06-17");

        Paciente paciente1 = new Paciente(1L, "Maria Joana", "11555596375", date1);
        Paciente paciente2 = new Paciente(2L, "Lucas Lopes", "12956999915", date2);
        Paciente paciente3 = new Paciente(3L, "Marcos Pacheco", "96315598371", date3);

        List<Paciente> pacientes = Arrays.asList(paciente1, paciente2, paciente3);
        when(pacienteService.getAllPacientes()).thenReturn(pacientes);

        mockMvc.perform(get("/api/pacientes"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.length()").value(pacientes.size()))
                .andExpect(jsonPath("$[0].nomePaciente").value("Maria Joana"))
                .andExpect(jsonPath("$[1].nomePaciente").value("Lucas Lopes"))
                .andExpect(jsonPath("$[2].nomePaciente").value("Marcos Pacheco"));
    }
}
