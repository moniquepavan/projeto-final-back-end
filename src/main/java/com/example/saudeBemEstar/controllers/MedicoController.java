package com.example.saudeBemEstar.controllers;

import com.example.saudeBemEstar.models.Medico;
import com.example.saudeBemEstar.services.MedicoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/medicos")
public class MedicoController {

    @Autowired
    private MedicoService medicoService;

    @GetMapping
    public ResponseEntity<List<Medico>> getAllMedicos(@RequestParam(defaultValue = "0") int page,
                                                      @RequestParam(defaultValue = "10") int size) {
        List<Medico> medicos = medicoService.findAll(page, size);
        return ResponseEntity.ok(medicos);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Medico> getMedicoById(@PathVariable Long id) {
        Medico medico = medicoService.findById(id);
        return medico != null ? ResponseEntity.ok(medico) : ResponseEntity.notFound().build();
    }

    @PostMapping
    public ResponseEntity<Medico> createMedico(@RequestBody Medico medico) {
        Medico createdMedico = medicoService.save(medico);
        return ResponseEntity.status(HttpStatus.CREATED).body(createdMedico);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Medico> updateMedico(@PathVariable Long id, @RequestBody Medico medicoDetails) {
        Medico existingMedico = medicoService.findById(id);
        if (existingMedico == null) {
            return ResponseEntity.notFound().build();
        }

        existingMedico.setNomeMedico(medicoDetails.getNomeMedico());
        existingMedico.setNumeroCRM(medicoDetails.getNumeroCRM());
        existingMedico.setEspecializacaoMedico(medicoDetails.getEspecializacaoMedico());

        Medico updatedMedico = medicoService.save(existingMedico);

        return ResponseEntity.ok(updatedMedico);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteMedico(@PathVariable Long id) {
        medicoService.deleteById(id);
        return ResponseEntity.ok("Médico excluído com sucesso");
    }
}
