package com.example.saudeBemEstar.models;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;

import java.util.Date;

@Entity

public class Medicacao {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idMedicacao;
    private String nomeMedicacao;
    private String fabricante;
    private String dosagem;
    private Date validadeMedicacao;

    public Medicacao() {
    }

    public Medicacao(Long idMedicacao, String nomeMedicacao, String fabricante, String dosagem, Date validadeMedicacao) {
        this.idMedicacao = idMedicacao;
        this.nomeMedicacao = nomeMedicacao;
        this.fabricante = fabricante;
        this.dosagem = dosagem;
        this.validadeMedicacao = validadeMedicacao;
    }

    public Long getIdMedicacao() {
        return idMedicacao;
    }

    public void setIdMedicacao(Long idMedicacao) {
        this.idMedicacao = idMedicacao;
    }

    public String getNomeMedicacao() {
        return nomeMedicacao;
    }

    public void setNomeMedicacao(String nomeMedicacao) {
        this.nomeMedicacao = nomeMedicacao;
    }

    public String getFabricante() {
        return fabricante;
    }

    public void setFabricante(String fabricante) {
        this.fabricante = fabricante;
    }

    public String getDosagem() {
        return dosagem;
    }

    public void setDosagem(String dosagem) {
        this.dosagem = dosagem;
    }

    public Date getValidadeMedicacao() {
        return validadeMedicacao;
    }

    public void setValidadeMedicacao(Date validadeMedicacao) {
        this.validadeMedicacao = validadeMedicacao;
    }
}


