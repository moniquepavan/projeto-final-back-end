package com.example.saudeBemEstar.models;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;

@Entity
public class Medico {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idMedico;
    private String nomeMedico;
    private Long numeroCRM;
    private String especializacaoMedico;

    public Medico() {
    }

    public Medico(Long idMedico, String nomeMedico, Long numeroCRM, String especializacaoMedico) {
        this.idMedico = idMedico;
        this.nomeMedico = nomeMedico;
        this.numeroCRM = numeroCRM;
        this.especializacaoMedico = especializacaoMedico;
    }

    public Long getIdMedico() {
        return idMedico;
    }

    public void setIdMedico(Long idMedico) {
        this.idMedico = idMedico;
    }

    public String getNomeMedico() {
        return nomeMedico;
    }

    public void setNomeMedico(String nomeMedico) {
        this.nomeMedico = nomeMedico;
    }

    public Long getNumeroCRM() {
        return numeroCRM;
    }

    public void setNumeroCRM(Long numeroCRM) {
        this.numeroCRM = numeroCRM;
    }

    public String getEspecializacaoMedico() {
        return especializacaoMedico;
    }

    public void setEspecializacaoMedico(String especializacaoMedico) {
        this.especializacaoMedico = especializacaoMedico;
    }

}
