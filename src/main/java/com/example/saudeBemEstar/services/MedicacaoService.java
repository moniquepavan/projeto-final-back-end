package com.example.saudeBemEstar.services;

import com.example.saudeBemEstar.models.Medicacao;
import com.example.saudeBemEstar.repositories.MedicacaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class MedicacaoService {

    @Autowired
    private MedicacaoRepository repository;

    public List<Medicacao> findAll(int page, int size) {
        return repository.findAll();
    }

    public Medicacao findById(Long id) {
        Optional<Medicacao> medicacaoOptional = repository.findById(id);
        return medicacaoOptional.orElse(null);
    }

    public Medicacao save(Medicacao medicacao) {
        return repository.save(medicacao);
    }

    public void deleteById(Long id) {
        repository.deleteById(id);
    }
}
