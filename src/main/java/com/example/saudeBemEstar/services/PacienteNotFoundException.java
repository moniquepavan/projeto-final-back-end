package com.example.saudeBemEstar.services;

public class PacienteNotFoundException extends RuntimeException {
    public PacienteNotFoundException(String id) {
        super("Paciente não encontrado com o ID: " + id);
    }
}

